CONTENTS OF THIS FILE
---------------------

 * Introduction
 * Requirements
 * Installation
 * Configuration

INTRODUCTION
------------

Currency Converter API allows to convert one currency to another.
It provides a block and plugin CurrencyConverterApiProvider.
At the moment there is only one provider,
it's Free Currency Converter API (https://free.currencyconverterapi.com).
But you can write your own plugin with a custom module.

REQUIREMENTS
------------

No special requirements.

INSTALLATION
------------

Install as you would normally install a contributed Drupal module. Visit
https://www.drupal.org/docs/8/extending-drupal-8/installing-drupal-8-modules
for further information.

CONFIGURATION
-------------

* Once you enable the module go to
  Configuration => Web services => Currency converter API and select a provider.
* In case you selected Free Currency Converter API
  go to https://free.currencyconverterapi.com/free-api-key and generate API key.
* Paste the key in the configuration field API key and save the form.
* Place a block Currency converter in any region.
* Configure enabled currencies and their order.

MAINTAINERS
-----------

Current maintainer:
 * Andriy Bakalyar - https://www.drupal.org/u/bakalyar
